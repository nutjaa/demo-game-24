import { Controller, Get, Query } from '@nestjs/common';
import { GooglePlaceService } from '../google/googlePlace.service';
import { Game24Service } from '../game24/game24.service';


@Controller('jenosize')
export class JenosizeController {
  constructor(
    private readonly googlePlaceService: GooglePlaceService,
    private readonly game24Service: Game24Service
  ) {}

  @Get('place-api')
  async getPlaceSearchResult(@Query('query') query): Promise<any> {
    return await this.googlePlaceService.search(query);
  }

  @Get('game24')
  getGame24Result(@Query('value') value): any {
    return this.game24Service.process(value);
  }
}
