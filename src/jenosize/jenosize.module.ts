import { Module } from '@nestjs/common';
import { JenosizeController } from './jenosize.controller';
import { JenosizeService } from './jenosize.service';
import { GoogleModule } from '../google/google.module';
import { Game24Module } from '../game24/game24.module';

@Module({
  imports: [GoogleModule, Game24Module],
  controllers: [JenosizeController],
  providers: [JenosizeService],
})
export class JenosizeModule {}
