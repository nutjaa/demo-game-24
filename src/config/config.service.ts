import * as dotenv from 'dotenv';
import * as fs from 'fs';

class ConfigService {
  private readonly envConfig: Record<string, string>;

  constructor(filePath: string) {
    this.envConfig = dotenv.parse(fs.readFileSync(filePath))
  }

  get(key: string): string {
    return this.envConfig[key];
  }
}

const configService = new ConfigService(`${process.env.NODE_ENV || 'development'}.env`);
export { configService };
