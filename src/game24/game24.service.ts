import { Injectable } from '@nestjs/common';
import { permutator } from '../helper/array.helper';
import { sprintf } from 'sprintf-js';

@Injectable()
export class Game24Service {

  process(input: [string]): any {

    const allPossibleInput = permutator(input);

    const allSyntax = [
      '%1$s %5$s %2$s %6$s %3$s %7$s %4$s',
      '(%1$s %5$s %2$s) %6$s %3$s %7$s %4$s',
      '(%1$s %5$s %2$s) %6$s (%3$s %7$s %4$s)',
      '((%1$s %5$s %2$s) %6$s %3$s) %7$s %4$s',
      '%1$s %5$s ((%2$s %6$s %3$s) %7$s %4$s)',
      '%1$s %5$s %2$s %6$s ( %3$s %7$s %4$s)'
    ];

    const operands = this.generateAllPossibleOpenrand();

    for (let i = 0; i < allPossibleInput.length; i++) {
      for (let j = 0; j < operands.length; j++) {
        for (let k = 0; k < allSyntax.length; k++) {
          const input = allPossibleInput[i].concat(operands[j]);
          const syntax = sprintf(allSyntax[k], ...input);
          if (eval(syntax) == 24) {
            console.log(syntax);
            return 'YES';
          }
        }
      }
    }

    return 'NO';
  }

  generateAllPossibleOpenrand() : any {
    const operands = ['+', '-', '*', '/'];

    const result = [];

    for (let i = 0; i < operands.length; i++) {
      for (let j = 0; j < operands.length; j++) {
        for (let k = 0; k < operands.length; k++) {
          const aValue = operands[i];
          const bValue = operands[j];
          const cValue = operands[k];
          result.push([aValue,bValue,cValue]);
        }
      }
    }

    return result
  }
}
