import { Module } from '@nestjs/common';
import { Game24Service } from './game24.service';

@Module({
  imports: [],
  providers: [Game24Service],
  exports: [Game24Service]
})
export class Game24Module {}
