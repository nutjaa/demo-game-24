import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';

import { JenosizeModule } from './jenosize/jenosize.module';

@Module({
  imports: [JenosizeModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
