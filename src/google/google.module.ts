import { Module, HttpModule } from '@nestjs/common';
import { GooglePlaceService } from './googlePlace.service';

@Module({
  imports: [HttpModule],
  providers: [GooglePlaceService],
  exports: [GooglePlaceService]
})
export class GoogleModule {}
