import { Injectable, HttpService } from '@nestjs/common';
import { configService } from '../config/config.service';
import { map } from 'rxjs/operators';

@Injectable()
export class GooglePlaceService {
  constructor(private httpService: HttpService){}

  async search(query: string): Promise<any> {
    const url = 'https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input='+query+'&inputtype=textquery&fields=photos,formatted_address,name,rating,opening_hours,geometry&key='+configService.get('GOOGLE_API');
    //console.log(url);
    // return await this.httpService.get(url);

    return this.httpService.get(url).pipe(map((res) => {
      return res.data;
    }));
  }
}
